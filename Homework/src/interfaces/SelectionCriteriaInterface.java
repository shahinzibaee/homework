package interfaces;

import java.util.List;

public interface SelectionCriteriaInterface {

	/**
	 * Called indirectly from SelectedPascalTriangleBuilder's constructor.
	 * @return    returns Fibonacci numbers list 
	 */
	List<Integer> getFibonacciNumbersList();

	/**
	 * Written for JUnit tests only. Getter for N.
	 * @return   returns the user-specified input integer N
	 */
	int getTestN();

	/**
	 * Called from makeSelectionCriteriaNumbersLists(). 
	 * Makes the Fibonacci number list for all numbers less than N.
	 */
	void makeFibonacciNumbersList();

	/**
	 * Called from constructor to make all selection criteria number lists.
	 */
	void makeSelectionCriteriaNumbersLists();

	/**
	 * Written for JUnit test only.
	 * Setter for N.
	 * @param N    user-specified input integer. 
	 */
	void setTestN(int N);

}

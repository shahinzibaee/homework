package interfaces;

public interface LauncherInterface {

	/**
	 * Launches the program. Takes user input from console.
	 */
	void launchProgram();

}

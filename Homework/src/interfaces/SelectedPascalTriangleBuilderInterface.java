package interfaces;

import java.util.List;

/** 
 * Computes Pascal's triangle and prints out selected rows according to methods in SelectedRows class.
 *  
 * @author Shahin
 *
 */
public interface SelectedPascalTriangleBuilderInterface {
		
	/**
	 * Adds row of Pascal's Triangle but only if row number satisfies selection  
	 * criteria - that the row number is less than the user-specified number N 
	 * and that the row number is also a Fibonacci number. 
	 * (Note: In Pascal's Triangle, a row number is by definition the same as the rows length)
	 * 
	 * @param nextRow                       a row of Pascal's triangle.
	 * @throws IllegalArgumentException     to signify timeout of in method's programmed delay when faced with an empty Fibonacci number list.
	 */
	void addToPascalTriangleSelectedRowsOnly(long[] nextRow) throws IllegalArgumentException;

	/**
	 * Builds Pascal's Triangle with selected rows, less than N, only. 
	 */
	void buildPascalTriangleSelectedRowsOnly();
	     
	/**
	 * calculates the next row down of Pascal's Triangle using only the previous row and the row number.
	 * @param row                           the current row of triangle being calculated.
	 * @param previousRowOfPascalTriangle   the previous row of triangle, necessary to calculate next (i.e. 'current') row.
	 * @return                              returns the current row of Pascal's Triangle.
	 */
	long[] calculateNextRowOfPascalTriangleFrom(long[] previousRowOfPascalTriangle);
	
	/**
	 * Written only for use in JUnit tests.
	 * @return    returns the FibonacciNumbersList
	 */
	List<Integer> getFibonacciNumbersList();
	
	/**
	 * 
	 * @return
	 */
	PascalTriangleInterface getPascalTriangle();
	
	/**
	 * Written only for use in JUnit tests.
	 * @return    returns N
	 */
	int getTestN();
	
	/**
	 * Method only used in JUnit tests. 
	 * @return     returns the triangle
	 */
	long[][] getTestPascalTriangleSelectedRowsOnly();
	
	/**
	 * 
	 * @return
	 */
	String getPrintablePascalTriangleSelectedRowsOnly();
	
	/**
	 * Initialises PascalFibonacciTriangle.
	 * @param rowLength    length of triangle to be initialised.
	 */
	void initPascalTriangle(int rowLength);

	/**
	 * Written only for use in JUnit tests.
	 * @param pascalTriangleSelectedRowsOnly     Pascal's Triangle including selected rows only. 
	 */
	void setPascalTriangle(long[][] testArray);
	

}

package interfaces;

public interface InputValidationInterface {

	/**
	 * Receives the user input and validates its format.
	 * @param input       the user input.
	 * @return            returns true if the format is numerical.
	 * @throws exception  thrown and caught if the input cannot be parsed to a number.
	 */
	boolean hasParsableNumericalFormat(String input);
	
	/**
	 * Checks that validated numerical user input is greater than 1. 
	 * @param parsableNumericalFormat   input already validated as numerical.
	 * @return                          returns true if the number is 2 or more.
	 */
	boolean isMoreThanOne(String parsableNumericalFormat);

	/**
	 * Checks that validated numerical user input is less than 2147483648.
	 * The maximum value of a Java integer is (2^31 - 1) which is 2147483647. 
	 * @param parsableNumericalFormat   input already validated as numerical.
	 * @return                          returns true if the number is 2147483647 or less.
	 */
	boolean isWithinJavaIntegerRange(String parsableNumericalFormat);
		
}

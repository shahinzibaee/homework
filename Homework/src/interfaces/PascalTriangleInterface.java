package interfaces;

public interface PascalTriangleInterface {

	/**
	 * Adds row to Pascal's Triangle.
	 * @param nextRowPreSelected   next row to add to triangle, which has laready been selected in different class
	 */
	void add(long[] nextRowPreSelected);

	/**
	 * Used by testSetNextRowIndex(int) JUnit test only. 
	 * @return
	 */
	int getTestNextRowIndex();
	 
	/**
	 * Method that is only used in JUnit tests.
	 * @return   returns selected rows of Pascal's Triangle
	 */
	long[][] getTestPascalTriangleSelectedRowsOnly();

	/**
	 * Written only for use in JUnit tests.
	 * @param pascalTriangleSelectedRowsOnly     Pascal's Triangle including selected rows only. 
	 */
	void setPascalTriangle(long[][] testTriangle);
	
	/**
	 * String representation of the triangle
	 * @return
	 */
	String toString();

	/**
	 *  
	 * @return
	 */
	void setNextRowIndex(int rowIndex);
	
}

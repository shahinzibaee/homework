package classes;

import interfaces.PascalTriangleInterface;

/**
 * Methods arranged in alphabetical order.
 * @author Shahin
 *
 */
public class PascalTriangle implements PascalTriangleInterface {
	
	private final int FIRST_ROW_NUMBER = 1;
	private final int FIRST_ROW_INDEX = 0;
	private final int FIRST_COLUMN_INDEX = 0;
	private final long FIRST_ROW_VALUE = 1L;
	private int nextRowIndex = 0;	
	private long[][] selectedRowsOnly = null;
	
	//constructor
	public PascalTriangle(int totalNumberOfSelectedRows) {

		setNextRowIndex(FIRST_ROW_INDEX);// just making it explicit
		initRowsLength(totalNumberOfSelectedRows);
		initFirstColumn();
		
	}

	@Override
	public void add(long[] nextRowPreSelected) {

		nextRowIndex++;
		initNextRow(nextRowPreSelected.length);
		selectedRowsOnly[nextRowIndex] = nextRowPreSelected;

	}

	@Override
	public int getTestNextRowIndex() {
		
		return nextRowIndex;
		
	}

	@Override
	public long[][] getTestPascalTriangleSelectedRowsOnly() {
		
		return selectedRowsOnly;
		
	}
	
	/**
	 * Initilases length of first row
	 * and assigns value of Pascal's Triangle first row, 1. 
	 */
	private void initFirstColumn() {

		initNextRow(FIRST_ROW_NUMBER);
		selectedRowsOnly[FIRST_ROW_INDEX][FIRST_COLUMN_INDEX] = FIRST_ROW_VALUE;

	}

	/**
	 * Initialises the new row of Pascal's Triangle to its base, 
	 * according to size of the new row's row number (length of row).
	 * @param rowNumber
	 */
	private void initNextRow(int rowNumber) {

		selectedRowsOnly[nextRowIndex] = new long[rowNumber];

	}

	/**
	 * Initialises rows as the number of distinct Fibonacci numbers
	 * less than user-specified input, N.   
	 * @param numberOfRows
	 */
	private void initRowsLength(int totalNumberOfRows) {

		selectedRowsOnly = new long[totalNumberOfRows][];	

	}

	/**
	 * Used for testing/debugging purposes only
	 */
	void print() {
		for (int i=0; i<selectedRowsOnly.length; i++) {
			for (int j=0; j<selectedRowsOnly[i].length; j++) {
				System.out.print(selectedRowsOnly[i][j] + " ");
			}
			System.out.print("\n");
		}
	}
	
	@Override
	public void setPascalTriangle(long[][] testTriangle) {
		
		selectedRowsOnly = testTriangle;
		
	}

	@Override
	public void setNextRowIndex(int rowIndex) {
		
		nextRowIndex = rowIndex;
	
	}
	
	@Override
	public String toString() {

		String triangle = "";
		
		for (int i = 0; i < selectedRowsOnly.length; i++) {

			if (selectedRowsOnly[i] != null) {

				for (int j = 0; j < selectedRowsOnly[i].length; j++) {

					triangle += selectedRowsOnly[i][j];
					
					if (j != selectedRowsOnly[i].length - 1) {
						
						triangle += " ";
						
					}

				}

			}

			triangle += "\n";

		}
		
		return triangle;

	}

}
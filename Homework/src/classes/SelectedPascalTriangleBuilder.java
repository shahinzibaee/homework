package classes;

import java.util.List;
import java.util.concurrent.TimeoutException;

import interfaces.InputValidationInterface;
import interfaces.SelectedPascalTriangleBuilderInterface;
import interfaces.PascalTriangleInterface;
import interfaces.SelectionCriteriaInterface;

/**
 * Methods are positioned below in alphabetical order.  
 * @author Shahin
 *
 */
public class SelectedPascalTriangleBuilder implements SelectedPascalTriangleBuilderInterface {
			
	private List<Integer> fibonacciNumbersList = null;
	private final int N;
	private PascalTriangleInterface pascalTriangle = null;
	private SelectionCriteriaInterface selectionCriteria = null;
	private InputValidationInterface inputValidation = null;
	
	/**
	 * constructor
	 * @param N    user-specified input integer
	 */
	public SelectedPascalTriangleBuilder(int N) {

		inputValidation = new InputValidation();

		try {

			if (!inputValidation.isMoreThanOne("" + N)) {

				throw new IllegalArgumentException("an N value less than 2 has been entered (probably via JUnit test)");

			}

			if (!inputValidation.isWithinJavaIntegerRange(""+ N)) {
			
				throw new IllegalArgumentException("an N value greater than Java's maximum integer value of 2,147,483,648) has been entered (probably via JUnit test)");
			
			}
			
		} catch (IllegalArgumentException iae) {
			
			System.out.println(iae.getMessage());
			
		}
		
		selectionCriteria = new SelectionCriteria(N);
		this.N = N;
		fibonacciNumbersList = getFibonacciNumbersList();		
			
	}

	/*
	 * Based on time measured using MacBook Pro, time taken for fibonacciNumbersList to be 
	 * filled up to and including N takes approx N / 7 millisecs.
	 * So my implementation includes a short proportional sleep() delay to enable this 
	 * thread to complete its job if the main thread finds it has not filled yet.
	 */
	@Override
	public void addToPascalTriangleSelectedRowsOnly(long[] nextRow) throws IllegalArgumentException {

		int attempts = 0;
		int rowNumber = nextRow.length;

		while (fibonacciNumbersList.isEmpty() && attempts < 10) {

			try {

				Thread.sleep(N / 5);

			} catch (InterruptedException e) {
				
				e.printStackTrace();
			
			}
			
			attempts++;

		}

		if (attempts == 10 && fibonacciNumbersList.isEmpty()) {
			
			throw new IllegalArgumentException("timed out: fibonacci number list is still empty after programmed delay of 10 x " + N + " / 5 milliseconds. Terminating program.");
			
		} else if (selectionCriteria.getFibonacciNumbersList().contains(rowNumber)) {

			pascalTriangle.add(nextRow);

		}
				
	}

	@Override
	public void buildPascalTriangleSelectedRowsOnly() {

		initPascalTriangle(howManyDistinctFibonacciNumbersLessThanN());
		long[] currentRow = null;
		long[] nextRow = {1L, 1L};
			
		if (N == 2) {

			return;

		} else {

			addToPascalTriangleSelectedRowsOnly(nextRow);
			currentRow = nextRow;

		}

		for (int rowNumber = 3; rowNumber <= N; rowNumber++) {
			
			nextRow = calculateNextRowOfPascalTriangleFrom(currentRow);
			addToPascalTriangleSelectedRowsOnly(nextRow);			
			currentRow = nextRow;

		}

	}
	
	@Override
	public long[] calculateNextRowOfPascalTriangleFrom(long[] currentRow) {
	
		long[] nextRow = new long[currentRow.length + 1];

		for (int column = 0; column <= nextRow.length; column++) { 
			
			nextRow[0] = nextRow[nextRow.length - 1] = 1;
			
			if (column >= 1 && column <= nextRow.length - 2) {
				
				nextRow[column] = currentRow[column - 1] + currentRow[column];
				
			}
			
		}

		return nextRow;
		
	}

	@Override
	public List<Integer> getFibonacciNumbersList() {
		
		int attempts = 0;

		while (selectionCriteria.getFibonacciNumbersList() == null && attempts < 11) {

			try {

				if (attempts == 10) {
					
					throw new TimeoutException("Timing out after 10 attempts at making list of Fibonacci Numbers");
					
				}
				
				Thread.sleep(N / 5);

			} catch (InterruptedException e) {
				
				e.printStackTrace();
			
			} catch (TimeoutException toe) {
			
				System.out.println(toe.getMessage());
				
			}
			
			attempts++;
			
		}

		return selectionCriteria.getFibonacciNumbersList();
		
	}
	
	@Override
	public int getTestN() {
	
		return N;
		
	}
	
	@Override
	public PascalTriangleInterface getPascalTriangle() {
		
		return pascalTriangle;
		
	}
	
	@Override
	public long[][] getTestPascalTriangleSelectedRowsOnly() throws IllegalArgumentException {

		if (pascalTriangle == null) {

			throw new IllegalArgumentException("Pascal's Triangle is not available yet");

		} 

		return pascalTriangle.getTestPascalTriangleSelectedRowsOnly();

	}
	
	@Override
	public String getPrintablePascalTriangleSelectedRowsOnly() {
		
		return pascalTriangle.toString();
		
	}
	
	public int howManyDistinctFibonacciNumbersLessThanN() {
//System.out.println("fib nums list null? "+fibonacciNumbersList);		
		return fibonacciNumbersList.size() - 1;
		
	}
	
	@Override
	public void initPascalTriangle(int totalNumberOfSelectedRows) {
		
		pascalTriangle = new PascalTriangle(totalNumberOfSelectedRows);
		
	}
	
	@Override
	public void setPascalTriangle(long[][] testArray) {
		
		pascalTriangle.setPascalTriangle(testArray);
		
	}

}
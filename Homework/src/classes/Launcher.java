package classes;

import interfaces.InputValidationInterface;
import interfaces.LauncherInterface;
import interfaces.SelectedPascalTriangleBuilderInterface;

public class Launcher implements LauncherInterface {

	public static void main(String[] args) {

		Launcher launcher = new Launcher();
		launcher.launchProgram();
		
	}
	
	@Override
	public void launchProgram() {
	
		InputValidationInterface inputValidation = new InputValidation();
		String input = "";
		
		do { 
			
			System.out.println("Input a positive number, greater than 1 and less than 2,147,483,648");
			System.out.println("(NOTE: currently 89 is the maximum input number you can enter without some output values in Pascal's Triangle exceeding the maximum for long (i.e. 9,223,372,036,854,775,807). I am planning to address this at some point!)");
			input = System.console().readLine();
			
		} while (!inputValidation.hasParsableNumericalFormat(input) || !inputValidation.isMoreThanOne(input) || !inputValidation.isWithinJavaIntegerRange(input));
		
		SelectedPascalTriangleBuilderInterface selectedPascalTriangleBuilder = new SelectedPascalTriangleBuilder(Integer.parseInt(input));
			
		try {

			selectedPascalTriangleBuilder.buildPascalTriangleSelectedRowsOnly();
			
		} catch (IllegalArgumentException iae) {
			
			System.out.println(iae.getMessage());
			
		}

		System.out.println("result: \n" + selectedPascalTriangleBuilder.getPrintablePascalTriangleSelectedRowsOnly());
		
	}
	
}

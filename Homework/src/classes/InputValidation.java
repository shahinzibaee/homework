package classes;

import interfaces.InputValidationInterface;

public class InputValidation implements InputValidationInterface {

	@Override
	public boolean hasParsableNumericalFormat(String input) {
		
		boolean isValid = true;
		
		try {
			
			Long.parseLong(input);
			
		} catch (NumberFormatException nfe) {
		
			System.out.println(input + " is not a valid numerical format");
			isValid = false;
			
		}
		
		return isValid;

	}
	
	@Override
	public boolean isMoreThanOne(String input) {
		
		return Long.parseLong(input) > 1;

	}
	
	@Override
	public boolean isWithinJavaIntegerRange(String input) {
		
		return Long.parseLong(input) <= Integer.MAX_VALUE;
		
	}
	
}

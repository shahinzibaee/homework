package classes;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import interfaces.InputValidationInterface;
import interfaces.SelectionCriteriaInterface;

/**
 * Holds and perform functions on the number lists used for selection of rows for output
 * (Currently has only Fibonacci numbers list.)
 * @author Shahin
 *
 */
public class SelectionCriteria implements SelectionCriteriaInterface {

	private int N = 0;
	private List<Integer> fibonacciNumbersList = null;
	private InputValidationInterface inputValidation = null;
	
	/**
	 * constructor
	 * @param N     user-specified input integer
	 */
	public SelectionCriteria(int N) {

		inputValidation = new InputValidation();

		try {

			if (!inputValidation.isMoreThanOne("" + N)) {

				throw new IllegalArgumentException("an N value less than 2 has been entered (probably via JUnit test)");

			}

			if (!inputValidation.isWithinJavaIntegerRange(""+ N)) {
			
				throw new IllegalArgumentException("an N value greater than Java's maximum integer value of 2,147,483,648) has been entered (probably via JUnit test)");
			
			}
			
		} catch (IllegalArgumentException iae) {
			
			System.out.println(iae.getMessage());
			
		}
		
		this.N = N;
		makeSelectionCriteriaNumbersLists();
		
	}
	
	/**
	 * Zero argument constructor, written for JUnit test only. 
	 */
	public SelectionCriteria() {}
	
	
	@Override
	public List<Integer> getFibonacciNumbersList() {
		
		int attempts = 0;
		
		while (fibonacciNumbersList == null && attempts < 6) {
//		
			attempts++;
	
				try {

					if (attempts == 5) {

						throw new TimeoutException("Timing out after 5 attempts at making list of Fibonacci Numbers");
						
					}
					
					Thread.sleep(N);

				} catch (InterruptedException e) {
					
					e.printStackTrace();
				
				} catch (TimeoutException toe) {
				
					System.out.println(toe.getMessage());
					
			}
				
		}

		return fibonacciNumbersList;
		
	}
		
	@Override
	public void makeFibonacciNumbersList() {

		Runnable fib = new FibonacciNumbersLessThanN();
		Thread t1 = new Thread(fib);
		t1.start();
				
	}
	
	@Override
	public void makeSelectionCriteriaNumbersLists() {

		makeFibonacciNumbersList();
		
	}

	@Override
	public int getTestN() {
		
		try {
			
			if (N < 0) {
			
				throw new Exception("N is less than 0. N should be positive"); 
		
			}
			
		} catch (Exception e) {
			
			System.out.println(e.getMessage());
			
		}
		
		return N;
	
	}
	
	@Override
	public void setTestN(int N) {

		this.N = N;
		
	}
		
	/**
	 * Fibonacci numbers are computed up to but not including the user-specified 
	 * number and stored in a list.
	 */
	public class FibonacciNumbersLessThanN implements Runnable {

		public void run() {
			
			synchronized(this) {

				fibonacciNumbersList = new ArrayList<>();
				fibonacciNumbersList.add(0, 1);
				fibonacciNumbersList.add(1, 1);
				int i = 2; 

				while (fibonacciNumbersList.get(i - 1) + fibonacciNumbersList.get(i - 2) < N) {
					
					fibonacciNumbersList.add(fibonacciNumbersList.get(i - 1) + fibonacciNumbersList.get(i - 2));
					i++;
					
				}
					
			}

		}

	}
	
}
package tests;

import static org.junit.Assert.*;
import interfaces.PascalTriangleInterface;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import classes.PascalTriangle;

/**
 * Written in the order in which they should be pass.
 * @author Shahin
 *
 */
public class PascalTriangleTests {

	PascalTriangleInterface pascalTriangle = null;
	SelectedPascalTriangleBuilderTests builderTests = null;
	
	@Before
	public void setUp() throws Exception {
	
		builderTests = new SelectedPascalTriangleBuilderTests(); 
	
	}

	@After
	public void tearDown() throws Exception {
	
		pascalTriangle = null;
		
	}

	
	@Test
	public void testSetNextRowIndex() {
		
		int randomTotalNumberOfSelectedRows = 5;
		pascalTriangle = new PascalTriangle(randomTotalNumberOfSelectedRows);
		
		int rowIndex = 4;
		pascalTriangle.setNextRowIndex(rowIndex);
		int actual = pascalTriangle.getTestNextRowIndex();
		int expected = 4;
		assertEquals(expected, actual);
		
		rowIndex = 2;
		pascalTriangle.setNextRowIndex(rowIndex);
		int actual2 = pascalTriangle.getTestNextRowIndex();
		int expected2 = 2;
		assertEquals(expected2, actual2);
	
	}
	 
	@Test
	public void testConstructor_int() {
		
		int randomTotalNumberOfSelectedRows = 5;
		pascalTriangle = new PascalTriangle(randomTotalNumberOfSelectedRows);
		int actualLength = pascalTriangle.getTestPascalTriangleSelectedRowsOnly().length;
		int expectedLength = 5;
		assertEquals(expectedLength, actualLength);

		long[][] pascalTriangleSelectedRowsOnly = pascalTriangle.getTestPascalTriangleSelectedRowsOnly();
		long actual00Value = pascalTriangleSelectedRowsOnly[0][0];
		long expected00Value = 1L;
		assertEquals(expected00Value, actual00Value);
	
		assertNull(pascalTriangleSelectedRowsOnly[1]);
		assertNull(pascalTriangleSelectedRowsOnly[2]);
		assertNull(pascalTriangleSelectedRowsOnly[3]);
		assertNull(pascalTriangleSelectedRowsOnly[4]);
			
	}

	@Test
	public void testAdd() {
		
		int randomTotalNumberOfSelectedRows = 2;
		pascalTriangle = new PascalTriangle(randomTotalNumberOfSelectedRows);
		long[] randomRow = {4L,5L,6L};
		pascalTriangle.add(randomRow);
		long[][] actual = pascalTriangle.getTestPascalTriangleSelectedRowsOnly();
		long[][] expected = {{1L},{4L,5L,6L}};
		assertTrue(builderTests.twoDArraysAreEqual(expected, actual));
		
		long[][] unexpected = {{2L},{4L,5L,6L}};
		assertFalse(builderTests.twoDArraysAreEqual(unexpected, actual));

	}

	@Test
	public void testGetNextRowsIndex() {

		int randomTotalNumberOfSelectedRows = 2;
		pascalTriangle = new PascalTriangle(randomTotalNumberOfSelectedRows);
		int expected = 0;
		int actual = pascalTriangle.getTestNextRowIndex();
		assertEquals(expected, actual);
		
		long[] randomRow = {4L,5L,6L};
		pascalTriangle.add(randomRow);//nextRowIndex gets increment in add(long[])
		
		expected = 1;
		actual = pascalTriangle.getTestNextRowIndex();
		assertEquals(expected, actual);

		expected = 2;
		actual = pascalTriangle.getTestNextRowIndex();
		assertNotEquals(expected, actual);
		
	}
	
	@Test
	public void testGetPascalTriangleSelectedRowsOnly() {
		
		int randomTotalNumberOfSelectedRows = 3;
		pascalTriangle = new PascalTriangle(randomTotalNumberOfSelectedRows);
		long[] randomRow = {4L,5L,6L};
		pascalTriangle.add(randomRow);
		long[] randomRow2 = {1L,1L,2L};
		pascalTriangle.add(randomRow2);
		
		long[][] expected = {{1L},{4L,5L,6L},{1L,1L,2L}};
		long[][] actual = pascalTriangle.getTestPascalTriangleSelectedRowsOnly();
		assertTrue(builderTests.twoDArraysAreEqual(expected, actual));

		long[][] unexpected = {{2L},{4L,5L,6L},{1L,1L,2L}};
		actual = pascalTriangle.getTestPascalTriangleSelectedRowsOnly();
		assertFalse(builderTests.twoDArraysAreEqual(unexpected, actual));
		
	}

	/**
	 * Written only for use in JUnit tests.
	 */
	@Test
	public void testSetPascalTriangle_long2DArray()  {

		int randomTotalNumberOfSelectedRows = 3;
		pascalTriangle = new PascalTriangle(randomTotalNumberOfSelectedRows);
		long[][] testAray = {{2L},{1L,1L},{1L,1L,1L}};
		pascalTriangle.setPascalTriangle(testAray);
		
		long[][] expected = {{2L},{1L,1L},{1L,1L,1L}};
		long[][] unexpected = {{1L},{1L,1L},{1L,1L,1L}};
		long[][] actual = pascalTriangle.getTestPascalTriangleSelectedRowsOnly();
		assertFalse(builderTests.twoDArraysAreEqual(unexpected, actual));	
		assertTrue(builderTests.twoDArraysAreEqual(expected, actual));		
				
	}

	@Test
	public void testToString() {

		int randomTotalNumberOfSelectedRows = 3;
		pascalTriangle = new PascalTriangle(randomTotalNumberOfSelectedRows);
		long[][] testAray = {{1L},{1L,1L},{1L,1L,1L}};
		pascalTriangle.setPascalTriangle(testAray);

		String expected = "1" + "\n" + "1 1" + "\n" + "1 1 1" + "\n";
		String actual = pascalTriangle.toString();
		assertEquals(expected, actual);

		print(testAray);
	
	}
	
	/**
	 * Just useful and reassuring to see arrays.
	 */
	void print(long[][] aray) {
		for (int i=0; i<aray.length; i++) {
			for (int j=0; j<aray[i].length; j++) {
				System.out.print(aray[i][j] + " ");
			}
			System.out.print("\n");
		}
	}

}

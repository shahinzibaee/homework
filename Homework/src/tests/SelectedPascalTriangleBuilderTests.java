package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import interfaces.SelectedPascalTriangleBuilderInterface;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import classes.SelectedPascalTriangleBuilder;

/**
 * Tests are written in order of their requirement to pass. 
 * @author Shahin
 */
public class SelectedPascalTriangleBuilderTests {
	
	private SelectedPascalTriangleBuilderInterface selectedPascalTriangleBuilder = null;
	private long start, end = 0;
	private int someRandomN = 0;
	
	@Before
	public void setUp() throws Exception {}

	@After
	public void tearDown() throws Exception {
		
		selectedPascalTriangleBuilder = null;
	
	}
	
	/**
	 * Constructor test
	 */
	@Test
	public void testConstructor_int() {
		
		someRandomN = 2;
		selectedPascalTriangleBuilder = new SelectedPascalTriangleBuilder(someRandomN);
		
		
	}
	
	/**
	 * MakeFibonacciNumberList() is needed by the next test method 
	 * addToselectedPascalTriangle_int_longArray(). 
	 * So, this must pass first.
	 */
	@Test
	public void testMakeFibonacciNumberList() {
		
		someRandomN = 6;
		SelectedPascalTriangleBuilderInterface selectedPascalTriangleBuilder = new SelectedPascalTriangleBuilder(someRandomN);
		List<Integer> actualFibonacciNumberList = selectedPascalTriangleBuilder.getFibonacciNumbersList();
		List<Integer> expectedFibonacciNumberList = new ArrayList<>();
		expectedFibonacciNumberList.add(1);
		expectedFibonacciNumberList.add(1);
		expectedFibonacciNumberList.add(2);
		expectedFibonacciNumberList.add(3);
		expectedFibonacciNumberList.add(5); 
		assertTrue(arrayListsAreEqual(expectedFibonacciNumberList, actualFibonacciNumberList));
		
		expectedFibonacciNumberList.add(8);
		assertFalse(arrayListsAreEqual(expectedFibonacciNumberList, actualFibonacciNumberList));

	}
	
	/**
	 * This test is purely to check getPascalTriangleSelectedRowsOnly() works as it 
	 * is used in other tests. In order for it be tested (and without using Mock objects)
	 * I've had to set the selectedPascalTriangle by writing a setter that will 
	 * only be used for testing.
	 */
	@Test
	public void testGetPascalTriangleSelectedRowsOnly() {
		
		long[][] selectedPascalTriangle = {{1L}, {1L, 1L}, {1L,2L,1L}};
		someRandomN = 1;		
		SelectedPascalTriangleBuilderInterface selectedPascalTriangleBuilder = new SelectedPascalTriangleBuilder(someRandomN);
		int randomNumber = 1;						  
		selectedPascalTriangleBuilder.initPascalTriangle(randomNumber);
		selectedPascalTriangleBuilder.setPascalTriangle(selectedPascalTriangle);
		
		long[][] actual2DArray = selectedPascalTriangleBuilder.getTestPascalTriangleSelectedRowsOnly();
		long[][] expected2DArray = selectedPascalTriangle;	
		assertTrue(twoDArraysAreEqual(expected2DArray,actual2DArray));
		
		long[][] unexpected2DArray = {{1L}, {1L, 1L}};
		assertFalse(twoDArraysAreEqual(unexpected2DArray,actual2DArray));
		
		long[][] unexpected2DArray2 = {{1L}, {2L, 1L}, {1L,2L,1L}};
		assertFalse(twoDArraysAreEqual(unexpected2DArray2,actual2DArray));
	
	}

	@Test(expected = IllegalArgumentException.class)
	public void IllegalArgumentExcepascalTriangleonTest() {

		someRandomN = 3;	
		selectedPascalTriangleBuilder = new SelectedPascalTriangleBuilder(someRandomN);
		selectedPascalTriangleBuilder.getTestPascalTriangleSelectedRowsOnly();

	}
	
	/*
	 * The following two tests are to test two methods called by buildPascalTriangleSelectedRowsOnly(). 
	 * So, these two tests must be written and must pass before buildPascalTriangleSelectedRowsOnly()
	 * can itself be tested. The two tests are for calculateCurrentRowOfPascalTriangleFrom(int, long[])
	 * and addToselectedPascalTriangle(int, long[]).
	 */
	
	@Test
	public void testCalculateCurrentRowOfPascalTriangleFrom_int_longArray() {
		
		someRandomN = 2;
		selectedPascalTriangleBuilder = new SelectedPascalTriangleBuilder(someRandomN);
		long[] previousRow = {1L,2L,1L};
		long[] actual = selectedPascalTriangleBuilder.calculateNextRowOfPascalTriangleFrom(previousRow);
		long[] expected = {1L, 3L, 3L, 1L};
		assertTrue(Arrays.equals(actual, expected));

		long[] expected2 = {1L, 3L, 2L, 1L};
		assertFalse(Arrays.equals(actual, expected2));
		
	}
	
	/**
	 * This test is for the second of two methods called by 
	 * buildPascalsFibonacciTriangleLessThanN(). 
	 * So, buildPascalsFibonacciTriangleLessThanN() is only tested 
	 * after these two tests pass. 
	 */
	@Test
	public void testAddToSelectedPascalTriangle_int_longArray() {
	
		someRandomN = 10;
		selectedPascalTriangleBuilder = new SelectedPascalTriangleBuilder(someRandomN);
		int totalNumberOfSelectedRows = 3;
		selectedPascalTriangleBuilder.initPascalTriangle(totalNumberOfSelectedRows);
		long[] nextRow = {1L, 3L, 1L};
		selectedPascalTriangleBuilder.addToPascalTriangleSelectedRowsOnly(nextRow);
		long[] nextRow2 = {1L, 3L, 1L};
		selectedPascalTriangleBuilder.addToPascalTriangleSelectedRowsOnly(nextRow2);
		long[][] expected2DArray = {{1L},{1L, 3L, 1L},{1L, 3L, 1L}};
		long[][] actual2DArray = selectedPascalTriangleBuilder.getTestPascalTriangleSelectedRowsOnly();
		assertTrue(twoDArraysAreEqual(expected2DArray, actual2DArray));
		
	}
	
	//for testing/debugging purposes only
	void print(long[][] array) {
		for (int i = 0; i<array.length; i++) {
			for (int j=0;j<array[i].length;j++) {
				System.out.print(array[i][j]+",");
			} 
			System.out.println();
		}
	}
	
	/**
	 * A JUnit test for a method that's only used by a JUnit test!
	 */
	@Test 
	public void testArrayListsAreEqual() {
		
		List<Integer> list1 = new ArrayList<>();
		List<Integer> list2 = new ArrayList<>();
		list1.add(3);
		list1.add(234);
		list2.add(3);
		list2.add(234);
		assertTrue(arrayListsAreEqual(list1, list2));
		
		list2.add(1);
		assertFalse(arrayListsAreEqual(list1, list2));
	
	}

	/**
	 * This method is available only to JUnit tests in this test class.
	 * @param arrayList1    an arrayList
	 * @param arrayList2    an arrayList
	 * @return              true if the contents of the array are the same values at the same positions.
	 */
	protected boolean arrayListsAreEqual(List<Integer> arrayList1, List<Integer> arrayList2) {

		boolean haveSameContents = true;
		int i = 0;
		
		if (arrayList1.size() != arrayList2.size()) {
			
			haveSameContents = false;
			
		}
		
		while (haveSameContents && i < arrayList1.size()) {

			if (!arrayList1.get(i).equals(arrayList2.get(i))) {
	
				haveSameContents = false;
				break;

			} else {
				
				i++;
				
			}

		}

		return haveSameContents;

	}
	/**
	 * A JUnit test for a method that's only used by a JUnit test!
	 */
	@Test 
	public void testTwoDArrayAreEqual() {
		
		long[][] array2D_1 = {{1L},{1L,1L},{1L,2L,1L},{1L,3L,3L,1L}};
		long[][] array2D_2 = {{1L},{1L,1L},{1L,2L,1L},{1L,3L,3L,1L}};
		assertTrue(twoDArraysAreEqual(array2D_1, array2D_2));

		long[][] array2D_3 = {{1L},{1L,1L},{1L,3L,1L},{1L,3L,3L,1L}};
		long[][] array2D_4 = {{1L},{1L,1L},{1L,2L,1L},{1L,3L,3L,1L}};
		assertFalse(twoDArraysAreEqual(array2D_3, array2D_4));

	}

	/**
	 * This method is available only to JUnit tests in this test class.
	 * @param <T>
	 * @param twoDArray1    a 2D array
	 * @param twoDArray2    a 2D array
	 * @return              true if the contents of the array are the same values at the same positions.
	 */
	protected boolean twoDArraysAreEqual(long[][] twoDArray1, long[][] twoDArray2) {

		boolean haveSameContents = true;
		int i = 0;

		if (twoDArray1.length != twoDArray2.length) {

			haveSameContents = false;
			
		} else {
		
			while (haveSameContents && i < twoDArray1.length && i < twoDArray2[i].length) {
		
				int j = 0;
			
				while (haveSameContents && j < twoDArray1[i].length && j < twoDArray2[i].length) {
				
					haveSameContents = twoDArray1[i][j] == twoDArray2[i][j];
					j++;
				
				}
			
				i++;
		
			}
		
		}
		
		return haveSameContents;
	
	}
	
	/**
	 * This test is primarily validated by eye and is designed also to observe 
	 * the duration of the computation.
	 */
	@Test
	public void testBuildPascalsFibonacciTriangleLessThanN() {
		
		someRandomN = 10;//[1,1,2,3,5,8]
		SelectedPascalTriangleBuilderInterface selectedPascalTriangleBuilder = new SelectedPascalTriangleBuilder(someRandomN);
		selectedPascalTriangleBuilder.buildPascalTriangleSelectedRowsOnly();

		long[][] actual2DArray = selectedPascalTriangleBuilder.getTestPascalTriangleSelectedRowsOnly();
		long[][] expected2DArray = {{1L},{1L,1L},{1L,2L,1L},{1L,4L,6L,4L,1L},{1L,7L,21L,35L,35L,21L,7L,2L}};
		assertFalse(twoDArraysAreEqual(expected2DArray,actual2DArray));
		
		long[][] expected2DArray2 = {{1L},{1L,1L},{1L,2L,1L},{1L,4L,6L,4L,1L},{1L,7L,21L,35L,35L,21L,7L,1L}};
		assertTrue(twoDArraysAreEqual(expected2DArray2,actual2DArray));
	
	}
	
	/**
	 * This test is primarily to find out how fast/slow the program runs 
	 * for different and, in particular, larger values of N.
	 */
	@Test
	public void testSpeedOfbuildPascalTriangleSelectedRowsOnly() {
	
		int N = 1;
		start = System.nanoTime();
		selectedPascalTriangleBuilder = new SelectedPascalTriangleBuilder(N);
		selectedPascalTriangleBuilder.buildPascalTriangleSelectedRowsOnly();
		end = System.nanoTime();
		System.out.println("time taken for N = " + N + " is: " + convertDurationFromNanoseconds(end - start) + "\n"); 
	
		N = 10;
		start = System.nanoTime();
		selectedPascalTriangleBuilder = new SelectedPascalTriangleBuilder(N);
		selectedPascalTriangleBuilder.buildPascalTriangleSelectedRowsOnly();
		end = System.nanoTime();
		System.out.println("time taken for N = " + N + " is: " + convertDurationFromNanoseconds(end - start) + "\n");
		
		N = 40;
		start = System.nanoTime();
		selectedPascalTriangleBuilder = new SelectedPascalTriangleBuilder(N);
		selectedPascalTriangleBuilder.buildPascalTriangleSelectedRowsOnly();
		end = System.nanoTime();
		System.out.println("time taken for N = " + N + " is: " + convertDurationFromNanoseconds(end - start) + "\n");

		N = 50;
		start = System.nanoTime();
		selectedPascalTriangleBuilder = new SelectedPascalTriangleBuilder(N);
		selectedPascalTriangleBuilder.buildPascalTriangleSelectedRowsOnly();
		end = System.nanoTime();
		System.out.println("time taken for N = " + N + " is: " + convertDurationFromNanoseconds(end - start) + "\n");

		N = 100;
		start = System.nanoTime();		
		selectedPascalTriangleBuilder = new SelectedPascalTriangleBuilder(N);
		selectedPascalTriangleBuilder.buildPascalTriangleSelectedRowsOnly();
		end = System.nanoTime();
		System.out.println("time taken for N = " + N + " is: " + convertDurationFromNanoseconds(end - start) + "\n");

		N = 200;
		start = System.nanoTime();
		selectedPascalTriangleBuilder = new SelectedPascalTriangleBuilder(N);
		selectedPascalTriangleBuilder.buildPascalTriangleSelectedRowsOnly();
		end = System.nanoTime();
		System.out.println("time taken for N = " + N + " is: " + convertDurationFromNanoseconds(end - start) + "\n");
	
		N = 500;
		start = System.nanoTime();
		selectedPascalTriangleBuilder = new SelectedPascalTriangleBuilder(N);
		selectedPascalTriangleBuilder.buildPascalTriangleSelectedRowsOnly();
		end = System.nanoTime();
		System.out.println("time taken for N = " + N + " is: " + convertDurationFromNanoseconds(end - start) + "\n");
		
	}

	/**
	 * This is not a test but is used by printSelectedRowsOfPascalsTriangleForLargeNTest() 
	 * @param duration    time taken for segment of code to run
	 * @return            returns the time with units 
	 */
	private String convertDurationFromNanoseconds(long duration) {

		String unitsOfTime = "nanoseconds";
		
		if (duration > 1000) {
			
			unitsOfTime = "microseconds";
			duration /= 1000;
			
			if (duration > 1000) {
				
				unitsOfTime = "milliseconds";
				duration /= 1000;
				
				if (duration > 1000) {
					
					unitsOfTime = "seconds";
					duration /= 1000;
					
				}
			
			}
		
		}
		
		return "" + duration + " " + unitsOfTime;
	
	}
	
}

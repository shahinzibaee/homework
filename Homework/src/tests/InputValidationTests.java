package tests;

import static org.junit.Assert.*;
import interfaces.InputValidationInterface;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import classes.InputValidation;

public class InputValidationTests {

	InputValidationInterface ivi = new InputValidation();
	String badFormatInput = "";
	String goodFormatInput = "";
	
	@Before
	public void setUp() throws Exception {
		
		badFormatInput = "8o0";
		goodFormatInput = "80";
		
	}

	@After
	public void tearDown() throws Exception {
	
		ivi = null;
		badFormatInput = "";
		goodFormatInput = "";
		
	}

	@Test
	public void testHasParsableNumericalFormat() {
		
		assertFalse(ivi.hasParsableNumericalFormat(badFormatInput));
		badFormatInput = "kjdf";
		assertFalse(ivi.hasParsableNumericalFormat(badFormatInput));
		badFormatInput = "4j4j4j";
		assertFalse(ivi.hasParsableNumericalFormat(badFormatInput));
		
		assertTrue(ivi.hasParsableNumericalFormat(goodFormatInput));
		goodFormatInput = "-80";
		assertTrue(ivi.hasParsableNumericalFormat(goodFormatInput));
		badFormatInput = "-80-";
		assertFalse(ivi.hasParsableNumericalFormat(badFormatInput));
		
	}
	
	@Test
	public void testIsWithinJavaIntegerRange_String() {
	
		String lessThan2147483648 = "2147483647";
		assertTrue(ivi.isWithinJavaIntegerRange(lessThan2147483648));

		lessThan2147483648 = "-2";
		assertTrue(ivi.isWithinJavaIntegerRange(lessThan2147483648));

		String notLessThan2147483648 = "2147483648";
		assertFalse(ivi.isWithinJavaIntegerRange(notLessThan2147483648));
		
	}
	
	@Test
	public void testIsMoreThanOne_String() {
		
		String moreThanOne = "2";
		assertTrue(ivi.isMoreThanOne(moreThanOne));

		moreThanOne = "2000000";
		assertTrue(ivi.isMoreThanOne(moreThanOne));

		String notMoreThanOne = "1";
		assertFalse(ivi.isMoreThanOne(notMoreThanOne));
		
		notMoreThanOne = "0";
		assertFalse(ivi.isMoreThanOne(notMoreThanOne));
		
		notMoreThanOne = "-2";
		assertFalse(ivi.isMoreThanOne(notMoreThanOne));

	}

}
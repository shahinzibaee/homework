package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import interfaces.SelectionCriteriaInterface;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import classes.SelectionCriteria;

/**
 * Written in the order the tests should pass.
 * @author Shahin
 *
 */
public class SelectionCriteriaTests {

	private SelectionCriteriaInterface selectionCriteria = null;
	private int testN = 0;
	private SelectedPascalTriangleBuilderTests builderTests = null;
	private List<Integer> testFibonacciNumbersList = null;
			
	@Before
	public void setUp() throws Exception {
		
		builderTests = new SelectedPascalTriangleBuilderTests();
		
	}

	@After
	public void tearDown() throws Exception {
	
		selectionCriteria = null;
	
	}

	/**
	 * Constructor calls testMakeSelectionCriteriaNumbersLists(), 
	 * which calls makeFibonacciNumbersList(), so this method is 
	 * being testing first. 
	 */
	@Test
	public void testMakeFibonacciNumbersList() {
		
		selectionCriteria = new SelectionCriteria();
		testN = 5;
		selectionCriteria.setTestN(testN);
		selectionCriteria.makeFibonacciNumbersList();
		testFibonacciNumbersList = new ArrayList<>();
		testFibonacciNumbersList.add(1);
		testFibonacciNumbersList.add(1);
		testFibonacciNumbersList.add(2);
		testFibonacciNumbersList.add(3);
		List<Integer> expected = testFibonacciNumbersList;
		List<Integer> actual = selectionCriteria.getFibonacciNumbersList();
		assertTrue(builderTests.arrayListsAreEqual(expected, actual));
		
		testFibonacciNumbersList.add(2);
		List<Integer> unexpected = testFibonacciNumbersList;
		assertFalse(builderTests.arrayListsAreEqual(unexpected, actual));
		
	}
	
	/**
	 * Constructor calls this method so this method is being testing first. 
	 */
	@Test
	public void testMakeSelectionCriteriaNumbersLists() {
	
		selectionCriteria = new SelectionCriteria();
		testN = 3;
		selectionCriteria.setTestN(testN);
		selectionCriteria.makeSelectionCriteriaNumbersLists();
		testFibonacciNumbersList = new ArrayList<>();
		testFibonacciNumbersList.add(1);
		testFibonacciNumbersList.add(1);
		testFibonacciNumbersList.add(2);
		List<Integer> expected = testFibonacciNumbersList;
		List<Integer> actual = selectionCriteria.getFibonacciNumbersList();
		assertTrue(builderTests.arrayListsAreEqual(expected, actual));

		testFibonacciNumbersList.add(2);
		List<Integer> unexpected = testFibonacciNumbersList;
		assertFalse(builderTests.arrayListsAreEqual(unexpected, actual));
		
	}
	
	@Test
	public void testConstructor_int() {
		
		testN = 2;
		selectionCriteria = new SelectionCriteria(testN);
		int actual = selectionCriteria.getTestN();
		int expected = 2;
		assertEquals(expected, actual);	
	
		testFibonacciNumbersList = new ArrayList<>();
		testFibonacciNumbersList.add(1);
		testFibonacciNumbersList.add(1);
		List<Integer> expected2 = testFibonacciNumbersList;
		List<Integer> actual2 = selectionCriteria.getFibonacciNumbersList();
		assertTrue(builderTests.arrayListsAreEqual(expected2, actual2));
		
		testFibonacciNumbersList.add(1);
		List<Integer> unexpected = testFibonacciNumbersList;
		assertFalse(builderTests.arrayListsAreEqual(unexpected, actual2));
		
	}
	
	/**
	 * Tests getFibonacciNumbersList() which is called from the builder constructor. 
	 */
	@Test
	public void testGetFibonacciNumbersList() {

		selectionCriteria = new SelectionCriteria();
		assertNull(selectionCriteria.getFibonacciNumbersList());
	
		testN = 5;
		selectionCriteria = new SelectionCriteria(testN);
		testFibonacciNumbersList = new ArrayList<>();
		testFibonacciNumbersList.add(1);
		testFibonacciNumbersList.add(1);
		testFibonacciNumbersList.add(2);
		testFibonacciNumbersList.add(3);
		List<Integer> expected = testFibonacciNumbersList;
		List<Integer> actual = selectionCriteria.getFibonacciNumbersList();
		assertTrue(builderTests.arrayListsAreEqual(expected, actual));
		
		testFibonacciNumbersList.add(3);
		List<Integer> unexpected = testFibonacciNumbersList;
		assertFalse(builderTests.arrayListsAreEqual(unexpected, actual));	
		
	}

}

Design Review
-------------

Look at our main application at http:/beta.archilogic.com and write a design review for it.

By design we mean (in ascending technicality order):

+ How it looks

+ How it behaves

+ How it works


The purpose is not finding glitches or bugs but rather evaluating the entire design of the
application from the perspective of the user and/or software engineer.

Focus on the areas which are most obvious or interesting to you and try to analyze their strong
and weak points (especially the weak ones).

The review should be short (0.5 - 2 pages), concise and easy to navigate.



Tool
----

#### Functional requirements

Create a command-line tool which will:

1. Accept a single positive integer number 'N' as an input.

2. Print only 'selected' rows of Pascal's triangle to standard output.

3. A row is considered 'selected' if its number is a Fibonacci number less than 'N'.

#### Other requirements

1. Provide your tool packaged in .zip archive.

2. Assume that the tool is will be tested by an end-user who:

   + Can unpack a .zip archive

   + Can read & write in english

   + Can download and install applications

   + Uses either Linux or Windows (choose one of these)

   + Will receive the archive only

   + Will be asked to test the archive contents

   + Will receive no other information

   + Tends to breaks things

3. The sources should be included in the archive along with reasonable amount of unit tests.

4. Use any languages, libraries or utilities you consider suitable to fulfill the requirements.

5. Use good design practices and your best judgement for anything not specified in the requirements.

6. Explain relevant design decisions.

Readme
--

The program gives a user the option to enter a number via the console. It then returns a Pascal's Triangle to the console. The Triangle, however, only includes those row numbers that are themselves Fibonacci numbers.

My design is intended to provide low coupling and extensibility for what is otherwise a relatively straight-forward program.

The only dependencies are JUnit (4.11 and hamcrest) and a JRE (1.7 was used).

The main method is in Launcher class.
